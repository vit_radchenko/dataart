package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VkLoginPage extends BasePage {
    private static By emailInputLocator = By.xpath("//input[@id='quick_email']");
    private static By passwordInputLocator = By.xpath("//input[@id='quick_pass']");
    private static By loginButtonLocator = By.xpath("//button[@id='quick_login_button']");

    public VkLoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected void waitForPageToLoad() {

    }

    public VkLoginPage open(){
        driver.get("asdasd");
        waitForPageToLoad();
        return this;
    }

    public VkLoginPage open(String url){
        driver.get(url);
        return this;
    }

    public VkLoginPage enterEmail(String text){
        typeText(emailInputLocator, text);
        return this;
    }

    public VkLoginPage enterPassword(String text){
        typeText(passwordInputLocator, text);
        return this;
    }

    public VkLoginPage clickOnLoginButton(){
        clickButton(loginButtonLocator);
        return this;
    }


}
