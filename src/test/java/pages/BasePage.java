package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public abstract class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
    }

    protected WebElement find(By locator){
        return driver.findElement(locator);
    }

    protected  void clickButton(final By locator){
        WebElement button = find(locator);
        button.click();
    }

    protected void typeText(final By locator, final String text){
        WebElement input = find(locator);
        input.clear();
        input.sendKeys(text);
    }

    protected void acceptAlert(){
        try {
            Alert alert = driver.switchTo().alert();
            alert.accept();
        } catch (Exception e){
            Assert.fail("Expected is not present");
        }
    }

    protected abstract void waitForPageToLoad();
}
