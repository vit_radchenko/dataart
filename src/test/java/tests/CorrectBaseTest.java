package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CorrectBaseTest {
    public static WebDriver driver;
    public static String url = "https://www.google.com.ua/";
    public static By inputLocator = By.xpath("//input[@id='lst-ib']");
    public static By searchButtonLocator = By.xpath("//button[@name='btnG']");
    public static String requestString  = "Котики";
    public static By resultsUrlsLocator = By.xpath("//cite");
    public static String expectedUrl = "https://kotiki.dasdairty.ru/";

    @BeforeMethod
    public void setUpBrowser(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown(){
        // close driver
        driver.quit();
    }


    @Test
    public void firsTest(){
        // test running
        driver.get(url);
        WebElement input = driver.findElement(inputLocator);
        input.clear();
        input.sendKeys(requestString);

        WebElement searchButton = driver.findElement(searchButtonLocator);
        searchButton.click();

        List<WebElement> resultList = driver.findElements(resultsUrlsLocator);
        List<String> urlResultList = new ArrayList<String>();

        //
        for (int i = 0; i < resultList.size(); i++){
            urlResultList.add(resultList.get(i).getText());
        }

        Assert.assertTrue(urlResultList.contains(expectedUrl), "Page doesn't have expected url: " + expectedUrl);
    }




}
