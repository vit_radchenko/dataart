package tests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.VkLoginPage;

public class VkTest extends BaseTest {
    public static String email = "vit.radchenko@ukr.net";
    public static String pass = "123testTest";
    public static String expectedUrl = "http://vk.com/id1143";


    @Test
    public void test(){
        VkLoginPage loginPage = new VkLoginPage(driver);

        loginPage.open("http://vk.com/")
        .enterEmail(email)
        .enterPassword(pass)
        .clickOnLoginButton();

        Assert.assertEquals(driver.getCurrentUrl(), expectedUrl, "Error message");
    }

}
